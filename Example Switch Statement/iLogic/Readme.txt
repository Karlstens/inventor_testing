This is important - one draw back from external rules is they don't execute upon parameter change - and external rules must be manually triggered to apply design changes. However, this workaround to this problem works well and is controlable enough - only down side is a local rule is needed in each master DoD design. Note that one local rule is required anyway to guarnetee iLogic LOD is active on assembly open. I'm sure there are other methods that also are best hosted locally. 

---------
'Activate the iLogic LOD
ThisApplication.ActiveDocument.ComponentDefinition.RepresentationsManager.LevelofDetailRepresentations("iLogic").Activate

'Allows iLogic local rule to execute if below params are toggled either via global form or directly in Inventor
iLogicVb.UpdateWhenDone = True

'By dummy mapping Assy User Params within this local script, they will then trigger this script to update when any of their values are updated. 
	temp_watch = Control_Pattern_A_Length_Count
	temp_watch = Pattern_A_Active
	temp_watch = Control_Colour
	temp_watch = Error_Checking
	temp_watch = Pattern_B_Active
	temp_watch = Control_Pattern_B_Length_Count

	Dim responsive_config As String = ".\Example Switch Statement\iLogic\Run_rules.iLogicVb"
	iLogicVb.RunExternalRule(responsive_config)

'If responsive config is decided to be disabled, Global forms will need the "Run_rules" ilogic button returned to execute changes.